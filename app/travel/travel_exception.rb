class TravelException < StandardError
  def initialize
    super('Ocurrió un error al calcular el viaje')
  end
end

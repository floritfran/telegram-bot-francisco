class TravelInfo
  def initialize(info)
    @info = info
  end

  def travel_time
    @info['routes'][0]['sections'][0]['summary']['duration']
  end
end

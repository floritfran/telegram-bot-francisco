class TravelAPI
  BASE_URL = 'https://router.hereapi.com'.freeze

  def initialize
    @connection = Faraday::Connection.new BASE_URL
  end

  def travel(latitud1, longitud1, latitud2, longitud2)
    origin = "#{latitud1}%2C#{longitud1}"
    destination = "#{latitud2}%2C#{longitud2}"
    routes_url = "/v8/routes?transportMode=car&origin=#{origin}&destination=#{destination}&return=summary&apikey=#{ENV['HERE_API_KEY']}"

    response = @connection.get routes_url
    response_json = JSON.parse(response.body)
    if response.success? && !response_json['routes'].empty?
      TravelInfo.new(response_json)
    else
      raise TravelException
    end
  end
end
